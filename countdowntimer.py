from PyQt4 import QtCore

class CountdownTimer(QtCore.QObject):
    tick = QtCore.pyqtSignal(float)
    timeout = QtCore.pyqtSignal()
    isFinished = False
    interval = 0.5

    def __init__(self):
        QtCore.QObject.__init__(self)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self._tick)

    def start(self, time):
        self.time = float(time)
        self.isFinished = False
        self.timer.start(self.interval * 1000)

    def stop(self):
        self.timer.stop()

    def _tick(self):
        self.time -= self.interval;

        self.tick.emit(self.time)

        if self.time == 0 :
            self.timer.stop()
            self.isFinished = True
            self.timeout.emit()
