from PyQt4 import QtCore
from itertools import cycle

class Blinker():
    def __init__(self, widget, blinkColor):
        self.widget = widget
        self.blinkColor = blinkColor

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self._tick)

        self.colors = ["Normal", self.blinkColor]
        self.colorsIterator = cycle(self.colors)

    def start(self):
        self.timer.start(250)

    def stop(self):
        self.timer.stop()
        self.widget.setStyleSheet("QWidget { background-color: Normal }" )

    def _tick(self):
        self.widget.setStyleSheet("QWidget { background-color: %s }" % self.colorsIterator.next())
