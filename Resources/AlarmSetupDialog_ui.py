# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AlarmSetupDialog.ui'
#
# Created: Fri Aug 16 13:09:30 2013
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DialogAlarmSetup(object):
    def setupUi(self, DialogAlarmSetup):
        DialogAlarmSetup.setObjectName(_fromUtf8("DialogAlarmSetup"))
        DialogAlarmSetup.resize(250, 115)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/Icons/bell.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        DialogAlarmSetup.setWindowIcon(icon)
        self.gridLayout = QtGui.QGridLayout(DialogAlarmSetup)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetMinAndMaxSize)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.labelWorkTime = QtGui.QLabel(DialogAlarmSetup)
        self.labelWorkTime.setObjectName(_fromUtf8("labelWorkTime"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelWorkTime)
        self.lineEditWorkTime = QtGui.QLineEdit(DialogAlarmSetup)
        self.lineEditWorkTime.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lineEditWorkTime.setObjectName(_fromUtf8("lineEditWorkTime"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditWorkTime)
        self.labelShortBreak = QtGui.QLabel(DialogAlarmSetup)
        self.labelShortBreak.setObjectName(_fromUtf8("labelShortBreak"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelShortBreak)
        self.lineEditShortBreak = QtGui.QLineEdit(DialogAlarmSetup)
        self.lineEditShortBreak.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lineEditShortBreak.setObjectName(_fromUtf8("lineEditShortBreak"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditShortBreak)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtGui.QDialogButtonBox(DialogAlarmSetup)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(DialogAlarmSetup)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), DialogAlarmSetup.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), DialogAlarmSetup.reject)
        QtCore.QObject.connect(self.lineEditShortBreak, QtCore.SIGNAL(_fromUtf8("returnPressed()")), DialogAlarmSetup.accept)
        QtCore.QObject.connect(self.lineEditWorkTime, QtCore.SIGNAL(_fromUtf8("returnPressed()")), DialogAlarmSetup.accept)
        QtCore.QMetaObject.connectSlotsByName(DialogAlarmSetup)
        DialogAlarmSetup.setTabOrder(self.lineEditWorkTime, self.lineEditShortBreak)

    def retranslateUi(self, DialogAlarmSetup):
        DialogAlarmSetup.setWindowTitle(_translate("DialogAlarmSetup", "Alarm Setup", None))
        self.labelWorkTime.setText(_translate("DialogAlarmSetup", "Work time", None))
        self.lineEditWorkTime.setToolTip(_translate("DialogAlarmSetup", "minutes", None))
        self.lineEditWorkTime.setText(_translate("DialogAlarmSetup", "0", None))
        self.labelShortBreak.setText(_translate("DialogAlarmSetup", "Short break", None))
        self.lineEditShortBreak.setToolTip(_translate("DialogAlarmSetup", "minutes", None))
        self.lineEditShortBreak.setText(_translate("DialogAlarmSetup", "0", None))

import icons_rc
