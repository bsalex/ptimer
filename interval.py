class Interval():
    def __init__(self):
        self.seconds = 0;
        self.color = "#FF0000";


    def initFromSerialized(self, serialized):
        serialized = serialized.trimmed()
        values = serialized.split(" ")

        self.seconds = values.takeFirst().toInt()[0] * 60
        self.color = values.takeFirst()
