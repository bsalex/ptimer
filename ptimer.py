#!/usr/bin/python
"""
ptimer - A reverse counting timer with multiple subsequent timers.

Usage:
ptimer.py val1 [val2 ...]

Example: (Starts a timer for 10mins and then for 15mins when 10mins expires)
    ptimer.py 10 15
"""
__author__ = "Amjith Ramanujam (amjith@gmail.com)"
__version__ = "$Revision: 0.1 $"
__date__ = "$Date: 2010/09/12 $"
__copyright__ = "Copyright (c) 2010, Amjith Ramanujam"

import sys
from itertools import cycle
from PyQt4 import QtCore, QtGui

from Resources.LcdNumber_ui import Ui_Form
from Resources.AlarmSetupDialog_ui import Ui_DialogAlarmSetup
from interval import *
from blinker import *
from countdowntimer import *

err = sys.stderr
_debug = 0
log = sys.stdout


class Timer(QtGui.QMainWindow):
    """
    The Timer class uses the QtTimer to keep count and uses a frameless window to display the count-down timer.
    A systray is also implemented with the option to toggle the timer, pause/play, reset, settings and quit.
    """
    def __init__(self, timer_values, parent=None):
        QtGui.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.timer = CountdownTimer()
        self.timer.tick.connect(self.updateTimerDisplay)
        self.timer.timeout.connect(self.startAlarm)

        self.blinker = Blinker(self.ui.lcdNumber, "Black")
        self.isPaused = False

        self.alarm_times = []
        self.settingsDialog = None

        settings = self.loadSettings()
        self.intervals = settings['intervals']

        if (len(self.intervals) > 0):
            self.resetTimer()
        else:
            self.settings()

        self.contextMenu = self.createMenu()

        self.trayIcon = QtGui.QSystemTrayIcon(self)
        self.trayIcon.setContextMenu(self.contextMenu)
        self.trayIcon.setIcon(QtGui.QIcon(':Icons/bell.png'))
        self.trayIcon.activated.connect(self.iconActivated)
        self.trayIcon.show()

    def loadSettings(self):
        settings = QtCore.QSettings()

        timeInterval = Interval()
        timeInterval.initFromSerialized(settings.value("pomodoro_time", "25 #FF0000").toString())

        pauseInterval = Interval()
        pauseInterval.initFromSerialized(settings.value("pomodoro_pause", "5 #00FF00").toString())

        return {'intervals' : [timeInterval, pauseInterval]}

    def createMenu(self):
        """
        This menu will be used as the context menu for the systray and the timer window.
        """
        contextMenu = QtGui.QMenu(self)

        toggleTimerAction = QtGui.QAction("&Toggle Timer", self,
                triggered=self.toggleTimer)
        pauseTimerAction = QtGui.QAction("&Pause/Play Timer", self,
                triggered=self.pauseTimer)
        resetTimerAction = QtGui.QAction("&Reset Timer", self,
                triggered=self.resetTimer)
        settingsAction = QtGui.QAction("&Settings", self,
                triggered=self.settings)
        quitAction = QtGui.QAction("&Quit", self,
                triggered=QtGui.qApp.quit)

        contextMenu.addAction(toggleTimerAction)
        contextMenu.addAction(pauseTimerAction)
        contextMenu.addAction(resetTimerAction)
        contextMenu.addSeparator()
        contextMenu.addAction(settingsAction)
        contextMenu.addSeparator()
        contextMenu.addAction(quitAction)

        return contextMenu

    def toggleTimer(self):
        """
        Toggles the display of the timer.
        """
        try:
            if self.ui.lcdNumber.isVisible():
                self.hide()
            else:
                self.show()
        except AttributeError:
            return

    def pauseTimer(self):
        if (self.isPaused == False):
            self.timer.stop()
            self.stopAlarm()
            self.isPaused = True
        else:
            self.resetTimer()
            self.isPaused = False

    def resetTimer(self):
        self.intervalsIterator = cycle(self.intervals)
        self.startTimer()

    def settings(self):
        if not self.settingsDialog:
            if (self.alarm_times):
                self.settingsDialog = SettingsDialog(self, self.alarm_times)
            else:
                self.settingsDialog = SettingsDialog(self)
        self.settingsDialog.show()
        self.connect(self.settingsDialog, QtCore.SIGNAL("Accept"), self.pullTimes)

    def pullTimes(self):
        self.alarm_times = [int(self.settingsDialog.ui.lineEditWorkTime.text()) * 60, int(self.settingsDialog.ui.lineEditShortBreak.text()) * 60 ]
        settings = QtCore.QSettings()
        settings.setValue("pomodoro_time", int(self.settingsDialog.ui.lineEditWorkTime.text()))
        settings.setValue("pause_time", int(self.settingsDialog.ui.lineEditShortBreak.text()))

        self.resetTimer()

    def startTimer(self):
        self.stopAlarm()
        self.currentInterval = self.intervalsIterator.next()
        self.timer.start(self.currentInterval.seconds)

    def showTimer(self):
        self.show()

    def updateTimerDisplay(self, currentTime):
        text = "%d:%02d" % (currentTime/60, currentTime % 60)
        self.ui.lcdNumber.display(text)
        self.ui.lcdNumber.setStyleSheet("QWidget { color: %s }" % self.currentInterval.color)

        self.ui.progressBar.setValue(round((1.0 - float(currentTime) / float(self.currentInterval.seconds)) * 1000))

        self.repaint()

    def startAlarm(self):
        self.showTimer()
        self.blinker.start()

    def stopAlarm(self):
        self.blinker.stop()

    def mousePressEvent(self, event):
        button = event.button()
        if button == 1:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft();


    def mouseReleaseEvent(self, event):
        button = event.button()
        if button == 1: # left click
            if (self.timer.isFinished):
                self.stopAlarm()
                self.startTimer()

        elif button == 2:
            self.contextMenu.exec_(QtGui.QCursor.pos())

    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)

    def iconActivated(self, reason):
        if reason in (QtGui.QSystemTrayIcon.Trigger, QtGui.QSystemTrayIcon.DoubleClick):
            self.toggleTimer()


class SettingsDialog(QtGui.QDialog):

    def __init__(self, parent=None, alarm_times = [25*60,5*60]):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowModality(QtCore.Qt.ApplicationModal)             # Make this a application blocking dialog.
        self.ui = Ui_DialogAlarmSetup()
        self.ui.setupUi(self)
        intValidator = QtGui.QIntValidator(0,99,self.ui.lineEditWorkTime) # create a int validator with range from 0 to 60
        self.ui.lineEditWorkTime.setValidator(intValidator);
        self.ui.lineEditShortBreak.setValidator(intValidator);
        self.ui.lineEditWorkTime.setText(str(alarm_times[0]/60))
        self.ui.lineEditShortBreak.setText(str(alarm_times[1]/60))
        self.ui.lineEditWorkTime.selectAll();
        self.ui.lineEditShortBreak.selectAll();

        self.trayMsgDisplayed = False
        self.show()

    def closeEvent(self, event):
        if self.trayMsgDisplayed == False:
            QtGui.QMessageBox.information(self, "Systray",
                    "The program will keep running in the system tray. To "
                    "terminate the program, choose <b>Quit</b> in the "
                    "context menu of the system tray entry.")
            self.hide()
            event.ignore()
            self.trayMsgDisplayed = True

    def reject(self):
        self.close();

    def accept(self):
        self.emit(QtCore.SIGNAL("Accept"))
        self.close();

def Str2Num(str_list):
    num = []
    for str in str_list:
        try:
            num.append(int(str))
        except ValueError:
            num.append(float(str))
    return num

def usage():
	print >>err, __doc__

def printTest():
    print "test printTest()"

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
        QtGui.QMessageBox.critical(None, "Systray",
                "No system tray on this system. Fail")
        sys.exit(1)
    QtGui.QApplication.setQuitOnLastWindowClosed(False)
    timerList = Str2Num(sys.argv[1:])
    myapp = Timer(timerList)
    sys.exit(app.exec_())

